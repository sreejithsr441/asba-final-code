//
//  WebViewController.h
//  ASBA
//
//  Created by Sabin.MS on 28/07/15.
//  Copyright (c) 2015 Jimmy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController<UIWebViewDelegate>
@property(nonatomic,strong)NSString *stringURL;
@property(nonatomic,strong)NSString *stringTitle;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
