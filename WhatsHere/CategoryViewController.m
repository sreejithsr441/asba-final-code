//
//  CategoryViewController.m
//  WhatsHere
//
//  Created by Rajkumar Sharma on 05/08/14.
//  Copyright (c) 2014 Jimmy. All rights reserved.
//

#import "CategoryViewController.h"
#import "ListViewController.h"
#import "WebViewController.h"

@interface CategoryViewController()

@end

@implementation CategoryViewController

- (void)viewDidLoad
{
    self.title = @"ASBA";
    [super viewDidLoad];

    //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar"]  forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"240x400"] forBarPosition:UIBarPositionTop barMetrics:UIBarMetricsDefault];
    
	// Do any additional setup after loading the view, typically from a nib.
}


-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = NO;
    //[[self.navigationItem ]UIBarPositionTopAttached];
    self.title=@"ASBA";
//    [[UINavigationBar appearance] setBackgroundImage:[[UIImage imageNamed:@"navBar"]
//                                                      resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch] forBarMetrics:UIBarMetricsDefault];
}

- (void)didReceiveMemoryWarning
{
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)categoryButtonTapped:(UIButton *)sender
{
        [self performSegueWithIdentifier:@"PushToWebView" sender:sender];
}

-(IBAction)nearByButtonTapped:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"" sender:sender];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"PushToWebView"]){
        UIButton *btn = (UIButton *)sender;
        WebViewController *controller = (WebViewController *)segue.destinationViewController;
        if (btn.tag==1){
            controller.stringURL = @"http://arsba.org/";
            controller.stringTitle=@"ASBA";
            UIBarButtonItem *backBtn=[[UIBarButtonItem alloc] initWithTitle:@"back" style:UIBarButtonItemStyleBordered target:nil action:nil];
            [[self navigationItem] setBackBarButtonItem:backBtn];
           // [self setTitle:@"ASBA"];
        }
        else if (btn.tag==2){
            controller.stringURL = @"http://arsba.org/training/events/";
             controller.stringTitle=@"Events";
            UIBarButtonItem *backBtn=[[UIBarButtonItem alloc] initWithTitle:@"back" style:UIBarButtonItemStyleBordered target:nil action:nil];
            [[self navigationItem] setBackBarButtonItem:backBtn];
           // [self setTitle:@"Upcoming"];
          
        }
        else if (btn.tag==3){
            controller.stringURL = @"http://arsba.org/asba/?page_id=890";
             controller.stringTitle=@"Training";
            UIBarButtonItem *backBtn=[[UIBarButtonItem alloc] initWithTitle:@"back" style:UIBarButtonItemStyleBordered target:nil action:nil];
            [[self navigationItem] setBackBarButtonItem:backBtn];
             // [self setTitle:@"News"];
     
        }

        else if (btn.tag==4){
            controller.stringURL = @"http://arsba.org/asba/?page_id=888";
             controller.stringTitle=@"Advocacy";
            UIBarButtonItem *backBtn=[[UIBarButtonItem alloc] initWithTitle:@"back" style:UIBarButtonItemStyleBordered target:nil action:nil];
            [[self navigationItem] setBackBarButtonItem:backBtn];
                   //[self setTitle:@"eMembership"];
         
        }

        else if (btn.tag==5){
            controller.stringURL = @"http://arsba.org/services/model-policies/";
             controller.stringTitle=@"Services";
            UIBarButtonItem *backBtn=[[UIBarButtonItem alloc] initWithTitle:@"back" style:UIBarButtonItemStyleBordered target:nil action:nil];
            [[self navigationItem] setBackBarButtonItem:backBtn];
               //[self setTitle:@"Forms"];
          
        }
        else if (btn.tag==6){
            controller.stringURL =@"http://arsba.org/asba/?page_id=896";
             controller.stringTitle=@"Resources";
            UIBarButtonItem *backBtn=[[UIBarButtonItem alloc] initWithTitle:@"back" style:UIBarButtonItemStyleBordered target:nil action:nil];
            [[self navigationItem] setBackBarButtonItem:backBtn];
              //[self setTitle:@"Advocacy"];
           
        }
        else if (btn.tag==7){
            controller.stringURL =@"http://arsba.org/?post_type=ib_educator_course";
             controller.stringTitle=@"News";
            UIBarButtonItem *backBtn=[[UIBarButtonItem alloc] initWithTitle:@"back" style:UIBarButtonItemStyleBordered target:nil action:nil];
            [[self navigationItem] setBackBarButtonItem:backBtn];
             //[self setTitle:@"Resources"];
           
        }
        else if (btn.tag==8){
            controller.stringURL = @"http://arsba.org/asba/?page_id=898";
             controller.stringTitle=@"About ASBA";
            UIBarButtonItem *backBtn=[[UIBarButtonItem alloc] initWithTitle:@"back" style:UIBarButtonItemStyleBordered target:nil action:nil];
            [[self navigationItem] setBackBarButtonItem:backBtn];
            //[self setTitle:@"About ASBA"];
        }
        else if (btn.tag==9){
            controller.stringURL = @"http://arsba.org/asba/?page_id=487";
             controller.stringTitle=@"Contact Us";
            UIBarButtonItem *backBtn=[[UIBarButtonItem alloc] initWithTitle:@"back" style:UIBarButtonItemStyleBordered target:nil action:nil];
            [[self navigationItem] setBackBarButtonItem:backBtn];
            //[self setTitle:@"Contact Us"];
        }

    
    }
}





@end
