//
//  SlidingMenuViewController.m
//  WhatsHere
//
//  Created by Rajkumar Sharma on 04/08/14.
//  Copyright (c) 2014 Jimmy. All rights reserved.
//

#import "SlidingMenuViewController.h"
#import "WebViewController.h"
#import "WebViewSecondViewController.h"

@interface SlidingMenuViewController ()


@property (weak, nonatomic) IBOutlet UIButton *btnHome;
@property (weak, nonatomic) IBOutlet UIButton *btnASBAdir;
@property (weak, nonatomic) IBOutlet UIButton *btnStaff;
@property (weak, nonatomic) IBOutlet UIButton *btnContact;
@property (weak, nonatomic) IBOutlet UIButton *btnEMembShp;
@property (weak, nonatomic) IBOutlet UIButton *btnFrms;

@property (nonatomic, weak) IBOutlet UIImageView *imgSideBar;
@end

@implementation SlidingMenuViewController

- (void)viewDidLoad
{
   // _imgSideBar.center = CGPointMake(_imgSideBar.center.x, _btnHome.center.y);
    [super viewDidLoad];
    switch (_btnToshowTag) {
        case 1:
            _btnHome.selected = NO;
            _btnASBAdir.selected = YES;
            _btnStaff.selected = NO;
            _btnContact.selected = NO;
             _imgSideBar.center=CGPointMake(_imgSideBar.center.x, _btnASBAdir.center.y);
            break;
            case 2:
            _btnHome.selected = NO;
            _btnASBAdir.selected = NO;
            _btnStaff.selected = YES;
            _btnContact.selected = NO;
              _imgSideBar.center=CGPointMake(_imgSideBar.center.x, _btnStaff.center.y);
            break;
            case 3:
            _btnHome.selected = NO;
            _btnASBAdir.selected = NO;
            _btnStaff.selected = NO;
            _btnContact.selected = YES;
            _imgSideBar.center=CGPointMake(_imgSideBar.center.x, _btnContact.center.y);
            break;

        default:
            _btnHome.selected = YES;
            _btnASBAdir.selected = NO;
            _btnStaff.selected = NO;
            _btnContact.selected = NO;
             _imgSideBar.center = CGPointMake(_imgSideBar.center.x, _btnHome.center.y);
            break;
    }
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIButton *)sender
{
    _btnHome.selected = NO;
    _btnASBAdir.selected = NO;
    _btnStaff.selected = NO;
    _btnContact.selected = NO;
    _btnEMembShp.selected=NO;
    _btnFrms.selected=NO;
    if ([segue.identifier isEqualToString:@"slidingToSettings"]||[segue.identifier isEqualToString:@"slidingToFind"])
    {
        _imgSideBar.hidden = NO;
        _imgSideBar.center = CGPointMake(_imgSideBar.center.x, sender.center.y);
        sender.selected =YES;
    }
    else //if([segue.identifier isEqualToString:@"pushFromSettToWeb"])
    {
        _imgSideBar.hidden = YES;
//        UIButton *btn=(UIButton *)sender;
//        _imgSideBar.hidden = YES;
//       WebViewController *controller = (WebViewController *)segue.destinationViewController;
//        if (btn.tag==1) {
//            controller.stringURL=@"http://aceonetest.com/asba/?page_id=1017";
//        }
//        if (btn.tag==2) {
//            controller.stringURL=@"http://aceonetest.com/asba/?page_id=1036";
//        }
//        if (btn.tag==3) {
//            controller.stringURL=@"http://aceonetest.com/asba/?page_id=487";
//        }
    }
}
- (IBAction)asbaDirectorsBoard:(id)sender
{
    _btnASBAdir.selected=YES;
    _btnHome.selected = NO;
    _btnStaff.selected = NO;
    _btnContact.selected = NO;
    _btnEMembShp.selected=NO;
    _btnFrms.selected=NO;

    WebViewSecondViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewSecondViewController"];
    controller.stringURL=@"http://arsba.org/asba/?page_id=1017";
    controller.strTitle=@"Directors";
    controller.btnTag=1;
    _imgSideBar.hidden=NO;
    _imgSideBar.center=CGPointMake(_imgSideBar.center.x, _btnASBAdir.center.y);
    //UIBarButtonItem *_backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:nil action:nil];
    //self.navigationItem.backBarButtonItem = _backButton;
self.title=@"Directors";
    [self.navigationController pushViewController:controller animated:YES];
    
}
- (IBAction)theStaffBackBtnClkd:(id)sender
{
    _btnASBAdir.selected=NO;
    _btnHome.selected = NO;
    _btnStaff.selected = YES;
    _btnContact.selected = NO;
    _btnEMembShp.selected=NO;
    _btnFrms.selected=NO;
    
    WebViewSecondViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewSecondViewController"];
    controller.stringURL=@"http://arsba.org/asba/?page_id=1036";
    controller.strTitle=@"Staff";
    controller.btnTag=2;
    _imgSideBar.hidden=NO;
    _imgSideBar.center=CGPointMake(_imgSideBar.center.x, _btnStaff.center.y);
   // self.title=@"Staff";
    [self.navigationController pushViewController:controller animated:YES];
}
- (IBAction)contactUsClkd:(id)sender
{
    _btnASBAdir.selected=NO;
    _btnHome.selected = NO;
    _btnStaff.selected = NO;
    _btnContact.selected = YES;
    _btnEMembShp.selected=NO;
    _btnFrms.selected=NO;
    WebViewSecondViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewSecondViewController"];
    controller.stringURL=@"http://arsba.org/asba/?page_id=487";
    controller.strTitle=@"Contact Us";
    controller.btnTag=3;
    _imgSideBar.hidden=NO;
    _imgSideBar.center=CGPointMake(_imgSideBar.center.x, _btnContact.center.y);
    //self.title=@"Contact Us";
    [self.navigationController pushViewController:controller animated:YES];
}
- (IBAction)BtnEMemberShipClkd:(id)sender
{
    _btnASBAdir.selected=NO;
    _btnHome.selected = NO;
    _btnStaff.selected = NO;
    _btnContact.selected = NO;
    _btnEMembShp.selected=YES;
    _btnFrms.selected=NO;
    WebViewSecondViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewSecondViewController"];
    controller.stringURL=@"https://em.eboardsolutions.com/Login.aspx?C=NfON";
    controller.strTitle=@"eMEMBERSHIP";
    controller.btnTag=4;
    _imgSideBar.hidden=NO;
    _imgSideBar.center=CGPointMake(_imgSideBar.center.x, _btnEMembShp.center.y);
    //self.title=@"Contact Us";
    [self.navigationController pushViewController:controller animated:YES];
    
}
- (IBAction)BtnFormsClkd:(id)sender
{
    _btnASBAdir.selected=NO;
    _btnHome.selected = NO;
    _btnStaff.selected = NO;
    _btnContact.selected = NO;
    _btnEMembShp.selected=NO;
    _btnFrms.selected=YES;
    WebViewSecondViewController *controller=[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewSecondViewController"];
    controller.stringURL=@"http://arsba.org/asba/?page_id=6168";
    controller.strTitle=@"FORMS";
    controller.btnTag=5;
    _imgSideBar.hidden=NO;
    _imgSideBar.center=CGPointMake(_imgSideBar.center.x, _btnFrms.center.y);
    //self.title=@"Contact Us";
    [self.navigationController pushViewController:controller animated:YES];
    
    
}


- (IBAction)unwindToMainMenuViewController:(UIStoryboardSegue *)segue{

}




@end
