//
//  WebViewSecondViewController.h
//  ASBA
//
//  Created by Sabin.MS on 29/07/15.
//  Copyright (c) 2015 Jimmy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewSecondViewController : UIViewController<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property(nonatomic,strong)NSString *stringURL;
@property(nonatomic,strong)NSString *strTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *labelHeading;
@property int btnTag;

@end
